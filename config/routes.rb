# -*- encoding : utf-8 -*-
Portalccsa::Application.routes.draw do
  resources :albums

  resources :notices, :only => [:show, :index]

  get '/contact' => 'contacts#show', as: :contact_form
  post '/contacts' => 'contacts#send_message'

  devise_for :users, :skip => :registrations, :controllers => { :confirmations => 'confirmations' }

  devise_scope :user do
    # Set pasword on account confimation
    put '/users/confirmation' => 'confirmations#update', :as => :update_user_confirmation

    # Editing user profile [because we aren't using registration] with default Devise actions
    get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put 'users' => 'devise/registrations#update', :as => 'user_registration'
  end

  # Dashboard needs to be defined out of namespace cause Devise is defined out of :admin namespace
  get '/admin/dashboard' => 'admin/dashboard#index', :as => :user_root

  get '/documents' => 'documents#index', as: :documents

  namespace :admin do
    root :to => 'dashboard#index'
    resources :users, :menus, :pages, :notices, :documents, :departments, :areas
  end

  controller :areas do
    get ':area', action: :show, as: :area
    get ':area/:menu/:page', action: :show_page, as: :area_menu_page
    get ':area/:menu/:unit', action: :show_unit, as: :area_menu_unit

    get ':area/:menu/:unit/:page', action: :show_unit_page, as: :area_menu_unit_page
    get ':area/:menu/:unit/notices', action: :show_unit_notices, as: :area_menu_unit_notices
    get ':area/:menu/:unit/notices/:notice_id', action: :show_unit_notice, as: :area_menu_unit_notice
  end

  root :to => 'site#home'
end
