# -*- encoding : utf-8 -*-

# # config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'portalccsa'
set :repo_url, 'git@bitbucket.org:brennovich/portalccsa.git'
set :deploy_to, "/home/lucas/apps/fetch(:application)"
set :scm, :git

set :branch, 'master'

set :keep_releases, 5

set :format, :pretty

set :log_level, :debug

set :pty, true

# set :ssh_options, {
#   forward_agent: true,
#   port: 3456
# }


set :linked_files, %w{config/database.yml config/config.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}


SSHKit.config.command_map[:rake]  = "bundle exec rake" #8
SSHKit.config.command_map[:rails] = "bundle exec rails"


namespace :deploy do

  desc "Restart application"
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join("tmp/restart.txt")
    end
  end

  after :finishing, "deploy:cleanup"

end

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# # Default deploy_to directory is /var/www/my_app
# # set :deploy_to, '/var/www/my_app'

# # Default value for :scm is :git
# # set :scm, :git

# # Default value for :format is :pretty
# # set :format, :pretty

# # Default value for :log_level is :debug
# # set :log_level, :debug

# # Default value for :pty is false
# # set :pty, true

# # Default value for :linked_files is []
# # set :linked_files, %w{config/database.yml}

# # Default value for linked_dirs is []
# # set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# # Default value for default_env is {}
# # set :default_env, { path: "/opt/ruby/bin:$PATH" }

# # Default value for keep_releases is 5
# # set :keep_releases, 5

# namespace :deploy do

#   desc 'Restart application'
#   task :restart do
#     on roles(:app), in: :sequence, wait: 5 do
#       # Your restart mechanism here, for example:
#       # execute :touch, release_path.join('tmp/restart.txt')
#     end
#   end

#   after :publishing, :restart

#   after :restart, :clear_cache do
#     on roles(:web), in: :groups, limit: 3, wait: 10 do
#       # Here we can do anything such as:
#       # within release_path do
#       #   execute :rake, 'cache:clear'
#       # end
#     end
#   end

# end


# ==================================================
# ==================================================

# require 'bundler/capistrano'
# require 'sidekiq/capistrano'

# set :rvm_ruby_string, :local              # use the same ruby as used locally for deployment
# set :rvm_autolibs_flag, "read-only" 

# # load "config/recipes/nodejs"
# load 'config/recipes/base'
# load 'config/recipes/check'
# load 'config/recipes/nginx'
# load 'config/recipes/postgresql'
# load 'config/recipes/rbenv'
# load 'config/recipes/unicorn'

# server 'ccsa.ufrn.br', :web, :app, :db, primary: true

# ssh_options[:keys] = ["#{ENV['HOME']}/.ssh/id_rsa"]
# default_run_options[:shell] = '/bin/bash --login'

# set :user, 'lucas'
# set :application, 'portalccsa'
# set :deploy_to, "/home/#{user}/apps/#{application}"
# set :deploy_via, :remote_cache
# set :use_sudo, false

# set :scm, 'git'

# set :repository, 'git@bitbucket.org:brennovich/portalccsa.git'
# set :remote, 'origin'
# set :branch, 'master'

# default_run_options[:pty] = true
# ssh_options[:forward_agent] = true

# after 'deploy', 'deploy:cleanup' # keep only the last 5 releases

# # postgres db pass ELnGJfiKUBPe88jPvAfgM
