# -*- encoding : utf-8 -*-
require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups(:assets => %w(development test))) if defined?(Bundler)

module Portalccsa
  class Application < Rails::Application
    config.time_zone = 'Brasilia'
    config.i18n.load_path += Dir[Rails.root.join('app', 'locales/**', '**.{rb,yml}').to_s]
    config.i18n.default_locale = :'pt-BR'
    config.encoding = 'utf-8'
    config.filter_parameters += [:password]
    config.active_support.escape_html_entities_in_json = true
    config.active_record.whitelist_attributes = true
    config.assets.enabled = true
    config.assets.version = '1.0'
    config.assets.initialize_on_precompile = false # Requirement for Heroku
    config.generators do |g|
      g.fixture_replacement :factory_girl, dir: "spec/factories"
      g.helper = false
      g.stylesheets = false
      g.javascripts = false
    end
  end
end

