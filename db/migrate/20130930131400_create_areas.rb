# -*- encoding : utf-8 -*-
class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.string :name
      t.string :color
      t.string :slug
      t.text :description

      t.timestamps
    end

    add_index :areas, :slug, :unique => true
  end
end
