# -*- encoding : utf-8 -*-
class CreateUnits < ActiveRecord::Migration
  def change
    create_table :units do |t|
      t.string :name
      t.string :slug
      t.text   :description
      t.references :menu
      t.references :category

      t.timestamps
    end

    add_index :units, :slug, :unique => true
  end
end
