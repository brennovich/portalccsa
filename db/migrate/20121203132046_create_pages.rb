# -*- encoding : utf-8 -*-
class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :name
      t.string :slug
      t.text :content
      t.integer :order_number
      t.references :pageable, :polymorphic => true

      t.timestamps
    end

    add_index :pages, :pageable_id
  end
end
