# -*- encoding : utf-8 -*-
class RenameTypeToRoleOfUsers < ActiveRecord::Migration
  def up
    rename_column :users, :type, :role
  end

  def down
    rename_column :users, :role, :type
  end
end
