# -*- encoding : utf-8 -*-
class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :name
      t.string :external_link
      t.string :slug
      t.references :area
      t.integer :order_number
      t.string :color

      t.timestamps
    end

    add_index :menus, :name
    add_index :menus, :slug
  end
end
