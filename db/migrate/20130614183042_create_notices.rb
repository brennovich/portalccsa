# -*- encoding : utf-8 -*-
class CreateNotices < ActiveRecord::Migration
  def change
    create_table :notices do |t|
      t.string :title
      t.text :body
      t.references :user
      t.references :unit
      t.string :slug
      t.string :category

      t.timestamps
    end

    add_index :notices, :slug, unique: true
    add_index :notices, :user_id
  end
end
