# -*- encoding : utf-8 -*-
class CreateAlbumMedia < ActiveRecord::Migration
  def change
    create_table :album_media do |t|
      t.string :title
      t.text :description
      t.references :album

      t.string :type

      t.string :video_url
      t.string :photo
      
      t.timestamps
    end
  end
end
