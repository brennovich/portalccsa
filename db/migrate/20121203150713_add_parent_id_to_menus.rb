# -*- encoding : utf-8 -*-
class AddParentIdToMenus < ActiveRecord::Migration
  def change
    add_column :menus, :parent_id, :integer
    add_index :menus, :parent_id
  end
end
