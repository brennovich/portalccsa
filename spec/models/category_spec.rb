# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Category do
  it { should validate_presence_of(:name) }

  it { should have_db_column(:slug).of_type(:string) }
  it { should have_db_index(:slug).unique(true) } 
  
  it { should have_many(:units) }
  it { should have_many(:pages) }

  it 'creates a menu with same name on create' do
    cat = Category.create(name: 'Pós Graduação')
    expect(Menu.find_by_name('Pós Graduação')).to be
  end

  it 'updates the menu name when its name is updated' do
    cat = Category.create(name: 'Pós Graduação')
    cat.name = 'Graduação'
    cat.save
    
    expect(Menu.find_by_name('Pós Graduação')).to be_nil
    expect(Menu.find_by_name('Graduação')).to be
  end
end
