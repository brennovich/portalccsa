# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Area do
  it { should validate_uniqueness_of(:name) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:color) }


  it { should have_db_column(:slug).of_type(:string) }
  it { should have_db_index(:slug).unique(true) } 
  
  it { should have_many(:categories) }
end
