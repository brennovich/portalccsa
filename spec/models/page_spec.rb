# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Page do
  let(:about) { create :about_page }

  it 'has a valid factory' do
    expect(about).to be_valid
  end

  it 'finds with slug' do
    expect(Page.find about.slug).to eql(about)
  end
end
