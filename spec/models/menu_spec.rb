# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Menu do
  let(:menu) { build_stubbed :capes }

  it 'has a valid factory' do
    expect(menu).to be_valid
  end

  it 'has many categories' do
    expect(menu.categories.new).to be_kind_of(Category)
  end

  it 'has many pages' do
    expect(menu.pages.new).to be_kind_of(Page)
  end

  it 'finds with slug' do
    Menu.should_receive(:find).with(menu.slug) { menu }
    expect(Menu.find menu.slug).to eql(menu)
  end

  it 'has parent and children' do
    expect(menu.parent).to be_kind_of(Menu)
    expect(menu.parent.children.new).to be_kind_of(Menu)
  end
end
