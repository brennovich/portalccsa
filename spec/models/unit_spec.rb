# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Unit do
  let(:ccsa) { create :ccsa }
  let(:ppga) { create :ppga }

  it 'has a valid factory' do
    expect(build :ccsa).to be_valid
  end

  it 'cannot be destroyed if was default' do
    ccsa
    Unit.last.destroy
    expect(Unit.last).to_not be_nil
  end

  it 'can be destroyed if was common' do
    ppga.destroy
    expect(Unit.last).to be_nil
  end

  it 'find with slug' do
    expect(Unit.find ccsa.slug).to eql(ccsa)
  end

  it 'should create a menu under the parent category' do
    cat = Category.create(name: 'Graduação')
    cat.save

    unit = Unit.new(name: 'Administração')
    unit.category = cat
    unit.save

    expect(Menu.find_by_name('Administração').parent).to eq(Menu.find_by_name('Graduação'))
  end

  it 'generated menu should have a link to unit page' do
    cat = Category.create(name: 'Graduação')
    cat.save

    unit = Unit.new(name: 'Administração')
    unit.category = cat
    unit.save

    expect(Menu.find_by_name('Administração').external_link).to eq unit_path(cat, unit)
  end
end
