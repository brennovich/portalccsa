# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :document do
    sequence(:name) { |i| "Document#{i}" }
    description 'Lorem ipsum'
    document { 
      Rack::Test::UploadedFile.new(
        File.join(Rails.root, 'spec', 'support', 'dummy_docs', 'dummy.pdf')) 
    }
  end
end
