# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :menu do
    factory :institucional do
      name 'Institucional'

      factory :mocked_menu do
        slug 'institucional'
      end
    end

    factory :research do
      name 'Pesquisa'
    end

    factory :capes do
      name 'Capes'
      external_link 'http://www.capes.gov.br'
      parent :factory => :research
    end
  end
end
