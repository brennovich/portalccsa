# -*- encoding : utf-8 -*-

FactoryGirl.define do
  factory :unit do
    description 'Lorem ipsum dolor sit amet, consectetur adipisicing.'

    factory :ccsa do
      name 'CCSA'
      association :category, :factory => :default
    end

    factory :ppga do
      name 'Administração'
      association :category, :factory => :pos
    end
  end
end
