# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :page do
    name 'Lorem ipsum'
    content 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

    factory :about_page do
      name 'About'
    end

    factory :page_with_menu do
      association :pageable, :factory => :institucional
    end

    factory :mocked_page do
      name 'Lorem ipsum'
      slug 'lorem-ipsum'
      association :pageable, :factory => :mocked_menu
    end
  end
end
