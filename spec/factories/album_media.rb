# -*- encoding : utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :album_medium, :class => 'AlbumMedia' do
    title "MyString"
    description "MyText"
  end
end
