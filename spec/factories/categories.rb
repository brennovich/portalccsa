# -*- encoding : utf-8 -*-

FactoryGirl.define do
  factory :category do
    factory :ensino do
      name 'Ensino'
    end

    factory :pos do
      name 'Pós-graduação'
    end

    factory :default do
      name 'CCSA'
    end
  end
end
