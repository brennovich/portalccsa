# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@factory.com" }
    password 'password'

    factory :johndoe do
      name 'John Doe'
      role 'content_manager'
      confirmed_at Time.now
    end

    factory :no_confirm do
      name 'John Doe'
      role 'content_manager'
    end

    factory :brenno do
      name 'Brenno Costa'
      role 'admin'
      confirmed_at Time.now
    end
  end
end
