# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Editing Page' do
  let(:admin) { create :brenno }
  let!(:about_page) { create :page }

  before do
    sign_in admin
    click_link t('dashboard.menu.pages')
    click_link(:href => edit_admin_page_path(about_page))
  end

  context 'with valid attributes' do
    before do
      fill_in t_attr(:name, :page), :with => 'Updated'
      click_button t_button(:update, :page)
    end

    scenario 'success' do
      expect(current_path).to eql(admin_page_path(Page.last))
      expect(page).to have_content(Page.last.name)
      expect(page).to have_content(t 'flash.actions.update.notice', :resource_name => t('activerecord.models.page'))
    end
  end

  context 'with invalid attributes' do
    before do
      fill_in t_attr(:name, :page), :with => ''
      click_button t_button(:update, :page)
    end

    scenario 'fails' do
      expect(current_path).to eql(admin_page_path about_page)
      expect(page).to have_css('.error')
      expect(page).to have_content(t 'flash.actions.update.alert', :resource_name => t('activerecord.models.page'))
    end
  end
end
