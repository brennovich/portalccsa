# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Creating Page' do
  let(:admin) { create :brenno }
  let(:about_page) { build :about_page }
  let!(:menu) { create :institucional }

  before do
    sign_in admin
    click_link t('dashboard.menu.pages')
    click_link t('dashboard.actions.create', :resource => t('activerecord.models.page'))
  end

  context 'with valid attributes', :js => true do
    before do
      fill_in t_attr(:name, :page), :with => about_page.name
      fill_in t_attr(:content, :page), :with => about_page.content

      # Active select box by clicking on radio [Action defined by portalCCSA.toggleInputDisabled]
      find('#page_pageable_type_menu').click

      select menu.name, :from => t_attr(:pageable_menu, :page)
      click_button t_button(:create, :page)
    end

    scenario 'success' do
      expect(current_path).to eql(admin_page_path(Page.last))
      expect(page).to have_content(about_page.name)
      expect(page).to have_content(t('flash.actions.create.notice', :resource_name => t('activerecord.models.page')))
    end
  end

  context 'with invalid attributes' do
    before do
      fill_in t_attr(:name, :page), :with => nil
      fill_in t_attr(:content, :page), :with => about_page.content
      click_button t_button(:create, :page)
    end

    scenario 'fails' do
      expect(current_path).to eql(admin_pages_path)
      expect(page).to have_css('.error')
      expect(page).to have_content(t('flash.actions.create.alert', :resource_name => t('activerecord.models.page')))
    end
  end
end
