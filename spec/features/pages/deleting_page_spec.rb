# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Deleting Menu' do
  let(:admin) { create :brenno }
  let!(:about_page) { create :page }

  before do
    sign_in admin
    click_link t('dashboard.menu.pages')
    click_link t('dashboard.actions.destroy')
  end

  scenario 'success' do
    expect(current_path).to eql(admin_pages_path)
    expect(page).to_not have_content(about_page.name)
    expect(page).to have_content(t 'flash.actions.destroy.notice', :resource_name => t('activerecord.models.page'))
  end
end

