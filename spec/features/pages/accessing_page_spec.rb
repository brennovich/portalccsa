# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Accessing a Page' do
  let!(:lorem_ipsum) { create :page_with_menu }

  scenario 'via menu' do
    visit root_path

    within '.dropdown' do
      click_link lorem_ipsum.name
    end

    expect(page).to have_content(lorem_ipsum.content)
  end

  context 'via direct path' do
    scenario 'success' do
      visit page_path(lorem_ipsum.pageable, lorem_ipsum)
      expect(page).to have_content(lorem_ipsum.content)
    end
  end
end

