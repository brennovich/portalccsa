# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Dynamic Menus' do
  let!(:menu) { create :capes }

  scenario 'shows up in main menu' do
    visit '/'

    # First level menu
    within '.main-menu' do
      expect(page).to have_content(menu.parent.name)
    end

    # Dropdown menu
    within '.main-menu ul li ul li' do
      expect(page).to have_content(menu.name)
    end
  end
end
