# -*- encoding : utf-8 -*-
require 'spec_helper'

describe 'Deleting Menu' do
  let(:admin) { create :brenno }
  let!(:menu) { create :institucional }

  before do
    sign_in admin
    click_link t('dashboard.menu.menus')
    click_link t('dashboard.actions.destroy')
  end

  it 'success' do
    expect(current_path).to eql(admin_menus_path)
    expect(page).to_not have_content(menu.name)
  end

  it 'displays a notice flash message' do
    expect(page).to have_content(t 'flash.actions.destroy.notice', :resource_name => t('activerecord.models.menu'))
  end
end
