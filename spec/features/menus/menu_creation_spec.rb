# -*- encoding : utf-8 -*-
require 'spec_helper'

feature 'Menu creation' do
  let(:admin) { create :brenno }
  let(:menu) { build :capes }
  let!(:parent_menu) { create :institucional}

  background do
    sign_in admin
    click_link t('dashboard.menu.menus')
    click_link t('dashboard.actions.create', :resource => t('activerecord.models.menu'))
  end

  context 'with valid attributes' do
    background do
      fill_in t_attr(:name, :menu), :with => menu.name
      fill_in t_attr(:external_link, :menu), :with => menu.external_link
      select parent_menu.name, :from => t_attr(:parent, :menu)
      click_button t_button(:create, :menu)
    end

    it 'success' do
      expect(current_path).to eql(admin_menu_path(Menu.last))
      expect(page).to have_content(menu.name)
    end

    it 'displays a notice flash message' do
      expect(page).to have_content(t 'flash.actions.create.notice', :resource_name => t('activerecord.models.menu'))
    end
  end

  context 'with invalid attributes' do
    before do
      fill_in t_attr(:name, :menu), :with => ''
      click_button t_button(:create, :menu)
    end

    it 'fails' do
      expect(current_path).to eql(admin_menus_path)
      expect(page).to have_css('.error')
    end

    it 'displays a alert flash message' do
      expect(page).to have_content(t 'flash.actions.create.alert', :resource_name => t('activerecord.models.menu'))
    end
  end
end
