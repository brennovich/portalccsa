# -*- encoding : utf-8 -*-
require 'spec_helper'

describe 'Editing Menu' do
  let(:admin) { create :brenno }
  let!(:menu) { create :institucional }

  before do
    sign_in admin
    click_link t('dashboard.menu.menus')
    click_link(:href => edit_admin_menu_path(menu))
  end

  context 'with valid attributes' do
    before do
      fill_in t_attr(:name, :menu), :with => 'Updated'
      click_button t_button(:update, :menu)
    end

    it 'success' do
      expect(current_path).to eql(admin_menu_path(Menu.last))
      expect(page).to have_content(Menu.last.name)
    end

    it 'displays a notice flash message' do
      expect(page).to have_content(t 'flash.actions.update.notice', :resource_name => t('activerecord.models.menu'))
    end
  end

  context 'with invalid attributes' do
    before do
      fill_in t_attr(:name, :menu), :with => ''
      click_button t_button(:update, :menu)
    end

    it 'fails' do
      expect(current_path).to eql(admin_menu_path menu)
      expect(page).to have_css('.error')
    end

    it 'displays a alert flash message' do
      expect(page).to have_content(t 'flash.actions.update.alert', :resource_name => t('activerecord.models.menu'))
    end
  end
end
