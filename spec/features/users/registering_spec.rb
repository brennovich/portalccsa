# -*- encoding : utf-8 -*-
require 'spec_helper'

describe 'Registering User' do
  let(:admin) { create :brenno }
  let(:user) { User.last }

  before do
    sign_in admin
    visit user_root_path
    click_link t('dashboard.menu.users')
    click_link t('dashboard.actions.create', :resource => t('activerecord.models.user'))
    fill_in t_attr(:name, :user), :with =>  'John Doe'
    fill_in t_attr(:email, :user), :with => 'johndoe@example.com'
    select(t_attr(:content_manager, 'user.roles'), :from => 'user_role')
    click_button t_button(:create, :user)
  end

  context 'pointview of admin' do
    it 'creates user successfully' do
      expect(current_path).to eql(admin_user_path(user))
      expect(page).to have_content(user.name)
    end

    it 'displays an explain message' do
      expect(page).to have_content(t 'flash.admin.users.create.notice')
    end
  end

  context 'pointview of user' do
    before do
      visit user_root_path
      click_link t('menu.sign_out')
      visit "/users/confirmation?confirmation_token=#{user.confirmation_token}"
    end

    context 'accessing confirmation page' do
      it 'success' do
        expect(page).to have_content(t 'heading.confirmation_advice', :user => user.name)
      end
    end

    context 'setting password' do
      before do
        fill_in t('simple_form.labels.defaults.password'), :with => 'new_password'
        fill_in t('simple_form.labels.defaults.password_confirmation'), :with => 'new_password'
        click_button t_button(:account_confirm)
      end

      it 'displays success confirmation message' do
        expect(page).to have_content(t 'devise.confirmations.confirmed')
      end

      it 'redirects logged user' do
        expect(current_path).to eql(user_root_path)
      end
    end
  end
end
