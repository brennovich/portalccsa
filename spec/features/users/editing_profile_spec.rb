# -*- encoding : utf-8 -*-
require 'spec_helper'

describe 'Editing User' do
  let(:admin) { create :brenno }
  let!(:user)  { create :johndoe }

  context 'profile' do
    before do
      sign_in user
      click_link t('dashboard.menu.edit_profile')
      fill_in t_attr(:name, :user), :with =>  'John Updated'
      fill_in t_label(:current_password), :with => 'password'
    end

    subject { click_button t_button(:update_profile, :user) }

    it 'does not display role select' do
      expect(page).to_not have_content(t_attr(:role, :user))
    end

    it 'shows updated profile' do
      subject
      expect(current_path).to eql(user_root_path)
      expect(page).to have_content(t 'devise.registrations.updated')
    end
  end

  context 'resource' do
    before do
      sign_in admin
      click_link t('dashboard.menu.users')
      click_link "edit_user_#{user.id}"
      fill_in t_attr(:name, :user), :with =>  'John Updated'
      click_button t_button(:update, :user)
    end

    it 'shows updated user' do
      expect(current_path).to eql(admin_user_path user)
      expect(page).to have_content('John Updated')
    end
  end
end
