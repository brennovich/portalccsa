# -*- encoding : utf-8 -*-
module IntegrationHelper
  def sign_in user, password = 'password'
    visit new_user_session_path
    fill_in t_attr(:email, :user), :with => user.email
    fill_in t_attr(:password, :user), :with => password
    click_button t_button(:sign_in)
  end
end
