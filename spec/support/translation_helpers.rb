# -*- encoding : utf-8 -*-
module TranslationHelpers
  def t_label field, scope = :defaults
    I18n.t field.to_s, :scope => "simple_form.labels.#{scope.to_s}"
  end

  def t_attr field, scope
    I18n.t field.to_s, :scope => "activerecord.attributes.#{scope.to_s}"
  end

  def t_button button, scope = :defaults
    I18n.t "helpers.submit.#{scope.to_s}.#{button.to_s}"
  end

  def t string, options = {}
    I18n.t string, options
  end

  def responder_flash action, resource, type
    I18n.t("flash.actions.#{action.to_s}.#{type.to_s}", :resource_name => I18n.t("activerecord.models.#{resource.to_s}"))
  end
end
