# -*- encoding : utf-8 -*-
module ControllerHelpers
  def stubbed_sign_in user
    request.env['warden'].stub :authenticate! => user
    controller.stub :current_user => user
  end
end
