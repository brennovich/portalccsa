# -*- encoding : utf-8 -*-
require 'spec_helper'

describe TranslationHelper do
  describe '.translated_user_roles' do
    let(:role_array) do
      [
        [I18n.t('activerecord.attributes.user.roles.admin'), 'admin'],
        [I18n.t('activerecord.attributes.user.roles.content_manager'), 'content_manager'],
        [I18n.t('activerecord.attributes.user.roles.unit_manager'), 'unit_manager']
      ]
    end

    it 'returns translated roles' do
      expect(translated_user_roles).to eql(role_array)
    end
  end

  describe '.t_with_resource' do
    let(:translation) do
      'Cadastrar Usuário'
    end

    it 'returns translation with custom resource' do
      expect(t_with_resource 'dashboard.actions.create', :user).to eql(translation)
    end
  end

  describe '.t_attr' do
    it 'returns right attribute translation' do
      expect(t_attr :password, :user).to eql(I18n.t :password, :scope => 'activerecord.attributes.user')
    end
  end
end
