# -*- encoding : utf-8 -*-
require 'spec_helper'

describe SessionHelper do
  describe '.session_links' do
    context 'when has logged user' do
      it 'returns a logout link' do
        self.stub!(:current_user).and_return(build_stubbed :brenno)
        expect(session_links).to match(/#{I18n.t('menu.sign_out')}/)
      end
    end

    context 'when hasn\'t logged user' do
      it 'returns a login link' do
        self.stub!(:current_user).and_return(nil)
        expect(session_links).to match(/#{I18n.t('menu.sign_in')}/)
      end
    end
  end
end
