# -*- encoding : utf-8 -*-
require 'spec_helper'

describe AreasController do

  describe "GET 'show'" do
    it "returns http success" do
      get 'show'
      response.should be_success
    end
  end

  describe "GET 'show_page'" do
    it "returns http success" do
      get 'show_page'
      response.should be_success
    end
  end

  describe "GET 'show_unit'" do
    it "returns http success" do
      get 'show_unit'
      response.should be_success
    end
  end

  describe "GET 'show_unit_page'" do
    it "returns http success" do
      get 'show_unit_page'
      response.should be_success
    end
  end

  describe "GET 'show_unit_notices'" do
    it "returns http success" do
      get 'show_unit_notices'
      response.should be_success
    end
  end

  describe "GET 'show_unit_notice'" do
    it "returns http success" do
      get 'show_unit_notice'
      response.should be_success
    end
  end

end
