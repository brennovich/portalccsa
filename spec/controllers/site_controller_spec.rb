# -*- encoding : utf-8 -*-
require 'spec_helper'

describe SiteController do
  describe 'GET#home' do
    it 'success' do
      get :home
      expect(response).to be_success
    end
  end
end
