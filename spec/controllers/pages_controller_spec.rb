# -*- encoding : utf-8 -*-
require 'spec_helper'

describe PagesController do
  let(:lorem_ipsum) { build_stubbed :mocked_page }
  let(:menu) { lorem_ipsum.pageable }

  describe 'GET#show' do
    subject { get :show, :menu => menu.slug, :page => lorem_ipsum.slug }

    it 'success' do
      Menu.should_receive(:find).with(menu.slug).and_return(menu)

      expect(subject).to be_success
    end

    it 'assigns @page' do
      Menu.should_receive(:find).with(menu.slug).and_return(menu)
      Page.should_receive(:where).with(:slug => lorem_ipsum.slug, :pageable_id => menu.id).and_return([lorem_ipsum])

      subject
      expect(assigns :page).to eql(lorem_ipsum)
    end
  end
end
