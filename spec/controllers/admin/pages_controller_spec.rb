# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::PagesController do
  let(:admin) { build :brenno }
  let(:user) { build :johndoe }

  describe 'GET#index' do
    let(:page) { build :page }

    subject { get :index }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @pages' do
        subject
        expect(assigns(:pages)).to eql(Page.all)
      end
    end
  end

  describe 'GET#show' do
    let(:page) { create :page }

    subject { get :show, :id => page.slug }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @page' do
        subject
        expect(assigns(:page)).to be_kind_of(Page)
        expect(assigns(:page)).to eql(Page.last)
      end
    end
  end

  describe 'GET#new' do
    subject { get :new }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @page' do
        subject
        expect(assigns(:page)).to be_kind_of(Page)
        expect(assigns(:page)).to be_new_record
      end
    end
  end

  describe 'GET#edit' do
    let(:page) { create :page }

    subject { get :edit, :id => page.slug }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @page' do
        subject
        expect(assigns(:page)).to eql(page)
      end
    end
  end

  describe 'POST#create' do
    subject { post :create, :page => attributes_for(:page) }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      context 'with valid attributes' do
        it 'success' do
          expect { subject }.to change(Page, :count).by(1)
        end

        it 'assigns @page' do
          subject
          expect(assigns(:page).name).to eql('Lorem ipsum')
        end
      end

      context 'with invalid attributes' do
        subject { post :create, :page => {} }

        it 'fails' do
          expect { subject }.to change(Page, :count).by(0)
        end

        it 're-renders :new template' do
          subject
          expect(assigns :page).to be_kind_of(Page)
          expect(assigns :page).to be_new_record
        end

        it 'displays alert message' do
          subject
          expect(flash[:alert]).to eql(responder_flash :create, :page, :alert)
        end
      end
    end
  end

  describe 'PUT#update' do
    let(:page) { create :page }

    subject { put :update, :id => page.slug, :page => { :name => 'Updated' } }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'fails' do
        expect(subject).to redirect_to(user_root_path)
        expect(Page.find(page.slug).name).not_to eql('Updated')
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        subject
        expect(assigns(:page).name).to eql('Updated')
        expect(flash[:notice]).to eql(responder_flash :update, :page, :notice)
      end
    end
  end

  describe 'DELETE#destroy' do
    let(:page) { create :page }

    subject { delete :destroy, :id => page.slug }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        page
        expect { subject }.to change(Page, :count).by(-1)
      end
    end
  end
end
