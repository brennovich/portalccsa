# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MenusController do
  let(:admin) { build :brenno }
  let(:user) { build :johndoe }

  describe 'GET#index' do
    let(:menu) { build :institucional }

    subject { get :index }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @menus' do
        subject
        expect(assigns(:menus)).to eql(Menu.all)
      end
    end
  end

  describe 'GET#show' do
    let(:menu) { create :institucional }

    subject { get :show, :id => menu.slug }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @menu' do
        subject
        expect(assigns(:menu)).to eql(menu)
      end
    end
  end

  describe 'GET#new' do
    subject { get :new }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @menu' do
        subject
        expect(assigns(:menu)).to be_kind_of(Menu)
        expect(assigns(:menu)).to be_new_record
      end
    end
  end

  describe 'GET#edit' do
    let(:menu) { create :institucional }

    subject { get :edit, :id => menu.slug }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @menu' do
        subject
        expect(assigns(:menu)).to eql(menu)
      end
    end
  end

  describe 'POST#create' do
    subject { post :create, :menu => attributes_for(:institucional) }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      context 'with valid attributes' do
        it 'success' do
          expect { subject }.to change(Menu, :count).by(1)
        end

        it 'assigns @menu' do
          subject
          expect(assigns(:menu).name).to eql('Institucional')
        end
      end

      context 'with invalid attributes' do
        subject { post :create, :menu => {} }

        it 'fails' do
          expect { subject }.to change(Menu, :count).by(0)
        end

        it 're-renders :new template' do
          subject
          expect(assigns :menu).to be_kind_of(Menu)
          expect(assigns :menu).to be_new_record
        end

        it 'displays alert message' do
          subject
          expect(flash[:alert]).to eql(responder_flash :create, :menu, :alert)
        end
      end
    end
  end

  describe 'PUT#update' do
    let(:menu) { create :institucional }

    subject { put :update, :id => menu.slug, :menu => { :name => 'Updated' } }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'fails' do
        expect(subject).to redirect_to(user_root_path)
        expect(Menu.find(menu.slug).name).not_to eql('Updated')
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        subject
        expect(assigns(:menu).name).to eql('Updated')
        expect(flash[:notice]).to eql(responder_flash :update, :menu, :notice)
      end
    end
  end

  describe 'DELETE#destroy' do
    let(:menu) { create :institucional }

    subject { delete :destroy, :id => menu.slug }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        menu
        expect { subject }.to change(Menu, :count).by(-1)
      end
    end
  end
end
