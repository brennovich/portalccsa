# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::UsersController do
  describe 'GET#index' do
    let(:admin) { build :brenno }
    let(:user) { create :johndoe }

    subject { get :index }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @users' do
        subject
        expect(assigns(:users)).to eql(User.all)
      end
    end
  end

  describe 'GET#show' do
    let(:admin) { create :brenno }
    let(:user) { create :johndoe }

    subject { get :show, :id => user }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      context 'accessing others accounts' do
        subject { get :show, :id => admin }

        it 'redirects' do
          expect(subject).to redirect_to(user_root_path)
        end
      end

      context 'accessing your account' do
        subject { get :show, :id => user }

        it 'success' do
          expect(subject).to be_success
        end

        it 'assigns @user' do
          subject
          expect(assigns(:user)).to eql(user)
        end
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }
      subject { get :show, :id => user }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @user' do
        subject
        expect(assigns(:user)).to eql(user)
      end
    end
  end

  describe 'GET#new' do
    let(:admin) { build :brenno }
    let(:user) { build :johndoe }
    subject { get :new }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @user' do
        subject
        expect(assigns(:user)).to be_kind_of(User)
        expect(assigns(:user)).to be_new_record
      end
    end
  end

  describe 'GET#edit' do
    let(:admin) { create :brenno }
    let(:user) { create :johndoe }

    subject { get :edit, :id => user }

    context 'as guest' do
      it 'redirects to sign in' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      context 'editing others accounts' do
        subject { get :edit, :id => admin }

        it 'redirects' do
          expect(subject).to redirect_to(user_root_path)
        end
      end

      context 'accessing your account' do
        subject { get :edit, :id => user }

        it 'success' do
          expect(subject).to be_success
        end

        it 'assigns @user' do
          subject
          expect(assigns(:user)).to eql(user)
        end
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }
      subject { get :edit, :id => user }

      it 'success' do
        expect(subject).to be_success
      end

      it 'assigns @user' do
        subject
        expect(assigns(:user)).to eql(user)
      end
    end
  end

  describe 'POST#create' do
    let(:admin) { build_stubbed :brenno }
    let(:user) { build_stubbed :user}

    subject { post :create, :user => attributes_for(:no_confirm) }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      context 'with valid attributes' do
        it 'success' do
          expect { subject }.to change(User, :count).by(1)
        end

        it 'assigns @user' do
          expect(assigns :user).to eql(User.last)
        end
      end

      context 'with invalid attributes' do
        subject { post :create, :user => {} }

        it 'fails' do
          expect { subject }.to change(User, :count).by(0)
        end

        it 're-renders :new template' do
          subject
          expect(assigns :user).to be_kind_of(User)
          expect(assigns :user).to be_new_record
        end

        it 'displays alert message' do
          subject
          expect(flash[:alert]).to eql(responder_flash :create, :user, :alert)
        end
      end
    end
  end

  describe 'PUT#update' do
    let(:admin) { create :brenno }
    let(:user) { create :johndoe }

    subject { put :update, :id => user, :user => { :name => 'Updated' } }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      context 'other user account' do
        subject do
          put :update, :id => admin, :user => { :name => 'Updated' }
        end

        it 'fails' do
          expect(subject).to redirect_to(user_root_path)
          admin.reload
          expect(admin.name).not_to eql('Updated')
        end
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        subject
        user.reload
        expect(user.name).to eql('Updated')
        expect(flash[:notice]).to eql(responder_flash :update, :user, :notice)
      end
    end
  end

  describe 'DELETE#destroy' do
    let(:admin) { create :brenno }
    let(:user) { create :johndoe }

    subject { delete :destroy, :id => user }

    context 'as guest' do
      it 'redirects' do
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context 'as user' do
      before { stubbed_sign_in user }

      it 'redirects' do
        expect(subject).to redirect_to(user_root_path)
      end
    end

    context 'as admin' do
      before { stubbed_sign_in admin }

      it 'success' do
        user
        expect { subject }.to change(User, :count).by(-1)
      end
    end
  end
end

