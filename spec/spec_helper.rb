# -*- encoding : utf-8 -*-
require 'spork'

Spork.prefork do
  # Loading more in this block will cause your tests to run faster. However,
  # if you change any configuration or code from libraries loaded here, you'll
  # need to restart spork for it take effect.

  # This file is copied to spec/ when you run 'rails generate rspec:install'
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
end

Spork.each_run do
  load "#{Rails.root}/config/routes.rb"
  Dir["#{Rails.root}/app/**/*.rb"].each {|f| load f}
  Dir["#{Rails.root}/lib/**/*.rb"].each {|f| load f}

  # This code will be run each time you run your specs.
  RSpec.configure do |config|
    config.mock_with :rspec

    config.use_transactional_fixtures = true
  end
end

# -*- encoding : utf-8 -*-
ENV["RAILS_ENV"] ||= 'test'

require 'simplecov'
SimpleCov.start('rails') { add_group 'Decorators', 'app/decorators' } if ENV["COVERAGE"] == 'true'

require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'capybara/rspec'
require 'capybara/poltergeist'
require 'rspec/autorun'

Capybara.javascript_driver = :poltergeist

Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  config.include ControllerHelpers, :type => :controller
  config.include Devise::TestHelpers, :type => :controller
  config.include FactoryGirl::Syntax::Methods
  config.include IntegrationHelper, :type => :feature
  config.include TranslationHelpers, :type => :controller
  config.include TranslationHelpers, :type => :feature
  config.infer_base_class_for_anonymous_controllers = false
  config.mock_with :rspec
  config.order = 'random'
  config.use_transactional_fixtures = true
end
