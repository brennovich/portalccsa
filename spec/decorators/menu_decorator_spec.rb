# -*- encoding : utf-8 -*-
require 'spec_helper'

describe MenuDecorator do
  let(:menu) { MenuDecorator.new build_stubbed(:capes) }

  describe '.has_parent?' do
    context 'when menu has a parent' do
      it 'returns true' do
        expect(menu.has_parent?).to be_true
      end
    end

    context 'when menu hasn\'t a parent' do
      it 'returns false' do
        expect(menu.parent.has_parent?).to be_false
      end
    end
  end

  describe '.has_children?' do
    context 'when menu has children' do
      before { menu.parent.stub(:children).and_return([1, 2, 3]) }

      it 'returns true' do
        expect(menu.parent.has_children?).to be_true
      end
    end

    context 'when menu hasn\'t children' do
      it 'returns false' do
        expect(menu.has_children?).to be_false
      end
    end
  end

  describe '.has_nested_pages?' do
    context 'when menu has nested pages' do
      before { menu.stub(:pages).and_return([1, 2, 3]) }

      it 'returns true' do
        expect(menu.has_nested_pages?).to be_true
      end
    end

    context 'when menu hasn\'t nested pages' do
      it 'returns false' do
        expect(menu.parent.has_nested_pages?).to be_false
      end
    end
  end

  describe '.top_level' do
    context 'when menu hasn\'t parent (consequently a parent)' do

      it 'returns rendered partial' do
        expect(menu.parent.top_level).to be_kind_of(String)
      end
    end

    context 'when menu has parent (consequently a child)' do
      it 'returns nil' do
        expect(menu.top_level).to be_nil
      end
    end
  end

  describe '.dropdown' do
    context 'when menu has children' do
      before { menu.parent.stub(:children).and_return([menu]) }

      it 'returns rendered partial' do
        expect(menu.parent.dropdown).to be_kind_of(String)
      end
    end

    context 'when menu has nested pages' do
      let(:menu) { MenuDecorator.new build_stubbed(:mocked_menu) }

      before do
        menu.stub(:parent).and_return(menu)
        menu.parent.stub(:pages).and_return([build_stubbed(:mocked_page)])
      end

      it 'returns rendered partial' do
        expect(menu.parent.dropdown).to be_kind_of(String)
      end
    end

    context 'when menu hasn\'t children or nested pages' do
      it 'returns nil' do
        expect(menu.top_level).to be_nil
      end
    end
  end

  describe '.nested_pages' do
    context 'when menu has nested pages' do
      let(:menu) { MenuDecorator.new build_stubbed(:mocked_menu) }

      before do
        menu.stub(:parent).and_return(menu)
        menu.parent.stub(:pages).and_return([build_stubbed(:mocked_page)])
      end

      it 'returns rendered partial' do
        expect(menu.parent.nested_pages).to be_kind_of(String)
      end
    end

    context 'when menu has parent (consequently a child)' do
      it 'returns nil' do
        expect(menu.nested_pages).to be_nil
      end
    end
  end
end
