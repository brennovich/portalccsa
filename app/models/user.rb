# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  # Macros
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :confirmable

  # Accessible Attributes
  attr_accessible :name, :role, :email, :password, :password_confirmation, :remember_me

  # Validations
  validates_presence_of :name, :role

  # Callbacks
  before_validation :generate_password_for_users_created_by_admins

  has_one :page
  
  # Available user roles
  def self.roles
    %w(admin content_manager unit_manager)
  end

  # Admins only set Name and Email for register a new user
  def generate_password_for_users_created_by_admins
    if !password && new_record?
      self.password = Devise.friendly_token.first(8)
    end
  end

  # Role Check
  def admin?
    role == 'admin'
  end

  def content_manager?
    role == 'content_manager'
  end

  # Override behaviour for custom ConfimationsController
  def only_if_unconfirmed
    pending_any_confirmation { yield }
  end

  # Set the password without knowing the current password used
  # in our confirmation controller
  def attempt_set_password(params)
    update_attributes(params)
  end
end
