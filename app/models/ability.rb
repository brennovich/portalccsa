# -*- encoding : utf-8 -*-
class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # Guest user

    can :manage, :all if user.admin?
    can [:show, :edit, :update], User, :id => user.id
    can :index, Admin::DashboardController if user.confirmed?

    #gerenciadores de conteúdo
    #"Gerenciar páginas" que tiverem o nome dele no caminho, "Gerenciar notícias" e "gerenciar documentos";
    can [:show, :edit, :update, :index], [Document, Notice] if user.role == "content_manager"
    
    if user.role == "content_manager"
      can [:show, :edit, :update, :index], Page do |page|
        true if page.user == user
      end
    end
    
    #Gerenciadores de página/unidade
    #"Gerenciar páginas" que tiverem o nome dele no caminho.
    if user.role == "unit_manager"
      can [:show, :edit, :update, :index], Page do |page|
        true if page.user == user
      end
    end

  end
end
