# -*- encoding : utf-8 -*-
class Notice < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :scoped, scope: :unit

  belongs_to :user
  attr_accessible :body, :title, :category

  scope :recent, order("created_at DESC")

  POSSIBLE_CATEGORIES ||= ["Teses e Dissertações", "Revista", "Próximos eventos"]

  validates :category, :inclusion => { :in => POSSIBLE_CATEGORIES }, :allow_blank => true

  belongs_to :unit
end
