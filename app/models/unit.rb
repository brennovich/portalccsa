# -*- encoding : utf-8 -*-
class Unit < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :scoped, scope: :menu

  after_create :create_default_pages

  # Accessible Attributes
  attr_accessible :name, :description

  # Validations
  validates_presence_of :name, :description

  # Relationships
  belongs_to :menu
  has_many :documents
  has_many :notices
  has_many :pages, as: :pageable

  def save
    transaction do
      m = Menu.find_by_name(self.name_was)

      if m
        m.name = self.name 
      else
        m = Menu.new(name: self.name)
        m.parent = Menu.find_by_name(menu.name)
        m.external_link = Rails.application.routes.url_helpers.unit_path(menu.name, self.name)
      end

      m.save
      super
    end
  end

  def destroy
    default? ? false : super
  end

  def default?
    category.name == 'CCSA'
  end
end
