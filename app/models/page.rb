# -*- encoding : utf-8 -*-
class Page < ActiveRecord::Base
  include Orderable
  extend FriendlyId
  friendly_id :name, use: :scoped, scope: :pageable

  # Relationships
  belongs_to :pageable, :polymorphic => true
  belongs_to :user

  # Accessible Attributes
  attr_accessible :content, :name, :pageable_id, :pageable_type, :user_id

  # Validations
  validates_presence_of :name
  validates_uniqueness_of :slug, :scope => :pageable_id

  def full_name
    pageable.full_name + " > " + name
  end

  def department
    Department.where(slug: slug).first
  end

  def color
    return department.color if department
  end
end
