# -*- encoding : utf-8 -*-
class Album < ActiveRecord::Base
  attr_accessible :date, :description, :name

  has_many :album_medias
end
