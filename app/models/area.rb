# -*- encoding : utf-8 -*-
class Area < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  attr_accessible :color, :name, :description

  validates_presence_of :name
  validates_uniqueness_of :name
  validates_presence_of :color

  has_many :menus

  scope :content_manager, where(role: "content_manager")


  def default?
    name == 'CCSA'
  end
end
