# -*- encoding : utf-8 -*-
class Contact < ActiveRecord::Base
  attr_accessible :email, :message, :name
end
