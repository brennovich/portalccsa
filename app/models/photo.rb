# -*- encoding : utf-8 -*-
class Photo < AlbumMedia
  mount_uploader :photo, PhotoUploader

  def url
    photo.url
  end
end
