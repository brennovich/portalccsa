# -*- encoding : utf-8 -*-
class Document < ActiveRecord::Base
  acts_as_taggable
  mount_uploader :document, DocumentUploader

  scope :by_tag, lambda { |tag| tagged_with(tag) }
  scope :by_name, lambda { |name| where("name like ?", "%#{name}%") }

  delegate :url, :to => :document

  attr_accessible :name, :description, :document, :tag_list

  validates_presence_of :name
  validates_uniqueness_of :name

  belongs_to :unit

  def save
    super
    add_tags_to_menus
  end

  private

  def add_tags_to_menus
    tags = Document.tag_counts_on('tags').map { |tag| tag.name }
    menus = Menu.pluck(:name)
    to_save = tags - menus

    doc_menu = Menu.find_by_name('Documentos')

    to_save.each do |menu|
      doc_menu.children << Menu.where(name: menu.titleize).first_or_create do |m|
        m.external_link = "/documents?tag=#{menu}"
      end
    end
  end
end
