# -*- encoding : utf-8 -*-
class Menu < ActiveRecord::Base
  include Orderable
  extend FriendlyId
  friendly_id :name, use: :scoped, scope: :area

  scope :main_menu, where(area_id: 1) #CCSA

  default_scope order(:area_id)

  # Relationships
  belongs_to :parent, :class_name => 'Menu'
  belongs_to :area
  has_many :units
  has_many :categories
  has_many :children, :class_name => 'Menu', :foreign_key => 'parent_id'
  has_many :pages, :as => :pageable

  # Accessible Attributes
  attr_accessible :name, :external_link, :priority_order, :parent_id, :color

  # Validations
  validates_presence_of :name

  def full_name
    return parent.full_name + " > " + name if parent
    
    return area.name + " > " + name if area

    return name
  end

  # Class Methods
  def self.except_menu_with id
    where('id not in (?)', id)
  end

  # Instance Methods
  def possible_parents
    persisted? ? self.class.except_menu_with(id) : self.class.all
  end
end
