# -*- encoding : utf-8 -*-
class AlbumMedia < ActiveRecord::Base
  attr_accessible :description, :title

  belongs_to :album
end
