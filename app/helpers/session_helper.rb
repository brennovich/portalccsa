# -*- encoding : utf-8 -*-
module SessionHelper
  def session_links
    if current_user
      link_to I18n.t('menu.sign_out'), destroy_user_session_path, :method => :delete
    else
      link_to I18n.t('menu.sign_in'), new_user_session_path
    end
  end
end
