# -*- encoding : utf-8 -*-
module TranslationHelper
  def translated_resource model
    t "activerecord.models.#{model.to_s}"
  end
  alias :tr :translated_resource

  def translated_user_roles
    User.roles.map do |role|
      [t("activerecord.attributes.user.roles.#{role}"), role]
    end
  end

  def t_with_resource scope, resource
    t(scope, :resource => t(resource, :scope => 'activerecord.models'))
  end

  def t_attr field, scope
    I18n.t field.to_s, :scope => "activerecord.attributes.#{scope.to_s}"
  end
end
