# -*- encoding : utf-8 -*-
module TextHelper
  def truncate(str, nchars = 160)
    strip_tags(str).truncate(nchars)
  end
end
