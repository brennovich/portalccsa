@portalCCSA = @portalCCSA || {}

@portalCCSA.dropdownMenuHelper = (selector)->
  $(selector).each () ->
    $(this).mouseenter ->
      $(this).find('ul').first().delay(200).stop('fx', true, true).fadeIn(200, 'swing')

    $(this).mouseleave ->
      $(this).find('ul').first().delay(200).stop('fx', true, true).fadeOut(200, 'swing')

    $(this).find('ul').first().mouseenter ->
      $(this).parent().find('a').first().addClass('hover')

    $(this).find('ul').first().mouseleave ->
      $(this).parent().find('a').first().removeClass('hover')
