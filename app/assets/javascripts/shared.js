//= require jquery
//= require jquery_ujs
//= require_tree ./carousel
//= require_tree ./shared/functions
//= require jquery.ad-gallery.min

var galleries = $('.ad-gallery').adGallery({
  effect: 'fade',
  width: 800,
  cycle: true,
  slideshow: {
    start_label: '',
    stop_label: '',

    // Wrap around the countdown
    countdown_prefix: '(', 
    countdown_sufix: ')',
  },
});

$(".ad-slideshow-start").addClass('fontawesome-play')
$(".ad-slideshow-stop").addClass('fontawesome-pause')

