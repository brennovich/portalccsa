//= require shared
//= require_tree ./dashboard/functions
//= require ./dashboard/domready
//= require ckeditor-jquery

CKEDITOR.editorConfig = (config) ->
  config.allowedContent = false;
  true