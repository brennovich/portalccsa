@portalCCSA = @portalCCSA || {}

@portalCCSA.toggleInputDisabled = (toggler = 'input[type=radio]', input = 'select') ->
  togglers = $(toggler)
  inputs = $(input)

  defineDisabledPropReferencedBy = (reference) ->
    reference.parent().find(input).attr('disabled', !reference.prop('checked'))

  togglers.each ->
    if $(this).prop('checked')
      defineDisabledPropReferencedBy $(this)

  togglers.change ->
    # Disable all specified inputs
    inputs.prop('disabled', 'disabled')
    # Enable toggler respective input
    defineDisabledPropReferencedBy $(this)