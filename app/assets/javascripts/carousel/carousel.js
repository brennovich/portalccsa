$(document).ready(function() {

/*  CarouFredSel: a circular, responsive jQuery carousel.
  Configuration created by the "Configuration Robot"
  at caroufredsel.dev7studios.com
*/
  $(".carousel").carouFredSel({
    width: "100%",
    height: 80,
    items: {
      visible: 10,
      start: "random",
      width: "variable",
      height: "variable"
    },
    scroll: 2,
    auto: true,
    align: "left",
    prev: {
      button: "#carousel_prev",
      key: "left"
    },
    next: {
      button: "#carousel_next",
      key: "right"
    }
  });

});