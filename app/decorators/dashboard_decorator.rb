# -*- encoding : utf-8 -*-
class DashboardDecorator < Draper::Decorator
  delegate_all
  decorates :user


  def salutation
    "Bem vindo, #{name}"
  end
end
