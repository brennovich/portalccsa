# -*- encoding : utf-8 -*-
class MenuDecorator < Draper::Decorator
  include Draper::LazyHelpers

  decorates :menu

  decorates_association :children, :with => MenuDecorator, :scope => :ascending
  decorates_association :parent, :with => MenuDecorator

  delegate :name, :area

  def pages
    source.pages.ascending
  end

  def color
    source.color || '#333'
  end

  def external_link
    source.external_link || '#'
  end

  def has_parent?
    parent.present?
  end

  def has_children?
    children.present?
  end

  def has_nested_pages?
    !pages.empty?
  end

  def top_level
    render 'shared/menu/top_level_menu', :menu => self unless has_parent?
  end

  def dropdown
    render 'shared/menu/dropdown_menu', :menu => self if has_children? || has_nested_pages?
  end

  def nested_pages
    render 'shared/menu/nested_pages', :menu => self if has_nested_pages?
  end
end

