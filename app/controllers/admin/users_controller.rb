# -*- encoding : utf-8 -*-
class Admin::UsersController < Admin::MainController
  load_and_authorize_resource

  def index
    @users = User.order('name ASC')
  end

  def show
    @user_temp = User.find(params[:id])
  end

  def new
    @user_temp = User.new
  end

  def edit
    @user_temp = User.find(params[:id])
  end

  def create
    @user_temp = User.new(user_params)
    @user_temp.skip_confirmation!
    @user_temp.save
    respond_with :admin, :users
  end

  def update
    @user_temp = User.find(params[:id])
    @user_temp.update_attributes(user_params)
    respond_with :admin, :users
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_with :admin, @user
  end

  private

  def user_params
    current_user.admin? ? params[:user] : params[:user].delete(:role)
  end
end
