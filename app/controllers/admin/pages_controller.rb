# -*- encoding : utf-8 -*-
class Admin::PagesController < Admin::MainController
  load_and_authorize_resource

  def index
    @pages = Page.order('name ASC')
  end

  def show
    @page = Page.find(params[:id])
  end

  def new
    @page = Page.new
  end

  def edit
    @page = Page.find(params[:id])
  end

  def create
    @page = Page.new(params[:page])
    @page.save
    respond_with :admin, @page
  end

  def update
    @page = Page.find(params[:id])
    @page.update_attributes(params[:page])
    respond_with :admin, @page
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy
    respond_with :admin, @page
  end
end
