# -*- encoding : utf-8 -*-
class Admin::DashboardController < Admin::MainController
  load_and_authorize_resource :class => self

  def index
  end
end
