# -*- encoding : utf-8 -*-
class Admin::MenusController < Admin::MainController
  load_and_authorize_resource

  def index
    @menus = Menu.order('name ASC')
  end

  def show
    @menu = Menu.find(params[:id])
  end

  def new
    @menu = Menu.new
  end

  def edit
    @menu = Menu.find(params[:id])
  end

  def create
    @menu = Menu.new(params[:menu])
    @menu.save
    respond_with :admin, @menu
  end

  def update
    @menu = Menu.find(params[:id])
    @menu.update_attributes(params[:menu])
    respond_with :admin, @menu
  end

  def destroy
    @menu = Menu.find(params[:id])
    @menu.destroy
    respond_with :admin, @menu
  end
end
