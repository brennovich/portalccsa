# -*- encoding : utf-8 -*-
class Admin::DocumentsController < Admin::MainController
  load_and_authorize_resource

  def index
    @documents = Document.order('name ASC')
  end

  def show
    @document = Document.find(params[:id])
  end

  def new
    @document = Document.new
  end

  def edit
    @document = Document.find(params[:id])
  end

  def create
    @document = Document.new(params[:document])
    @document.save
    respond_with :admin, @document
  end

  def update
    @document = Document.find(params[:id])
    @document.update_attributes(params[:document])
    respond_with :admin, @document
  end

  def destroy
    @document = Document.find(params[:id])
    @document.destroy
    respond_with :admin, @document
  end
end
