# -*- encoding : utf-8 -*-
class Admin::NoticesController < Admin::MainController
  load_and_authorize_resource

  def index
    @notices = Notice.all
    respond_with :admin, @notices
  end

  def show
    @notice = Notice.find(params[:id])
    respond_with(@notice)
    page_title :title => @notice.title
  end

  def new
    @notice = Notice.new
    respond_with(@notice)
  end

  def edit
    @notice = Notice.find(params[:id])
  end

  def create
    @notice = Notice.new(params[:notice])
    @notice.user = current_user
    @notice.save
    respond_with :admin, @notice 
  end

  def update
    @notice = Notice.find(params[:id])
    @notice.update_attributes(params[:notice])
    respond_with :admin, @notice
  end

  def destroy
    @notice = Notice.find(params[:id])
    @notice.destroy
    respond_with :admin, @notice
  end
end
