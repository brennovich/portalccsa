# -*- encoding : utf-8 -*-
class Admin::MainController < ApplicationController
  layout 'dashboard'
  before_filter :authenticate_user!
  before_filter :load_user

  # CanCan Exception Handling
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to user_root_path, :alert => exception.message
  end

  def load_user
    @user = DashboardDecorator.new(current_user)
  end
end

