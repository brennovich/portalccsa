# -*- encoding : utf-8 -*-
class Admin::DepartmentsController < Admin::MainController
  load_and_authorize_resource

  def index
    @departments = Department.order('name ASC')
  end

  def show
    @department = Department.find(params[:id])
  end

  def new
    @department = Department.new
  end

  def edit
    @department = Department.find(params[:id])
  end

  def create
    @department = Department.new(params[:department])
    @department.save
    respond_with :admin, @department
  end

  def update
    @department = Department.find(params[:id])
    @department.update_attributes(params[:department])
    respond_with :admin, @department
  end

  def destroy
    @department = Department.find(params[:id])
    @department.destroy
    respond_with :admin, @department
  end
end
