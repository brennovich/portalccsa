# -*- encoding : utf-8 -*-
class NoticesController < ApplicationController
  layout 'site'

  def show
  	@notice = Notice.find(params[:id])
    page_title :title => @notice.title
  end

  def index
    @notices = Notice.order('created_at DESC')
  end
end
