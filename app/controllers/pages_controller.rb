# -*- encoding : utf-8 -*-
class PagesController < ApplicationController
  layout 'site'

  def show
    menu = Menu.find(params[:menu])
    @page = Page.where(:slug => params[:page], :pageable_id => menu.id).first
  end
end
