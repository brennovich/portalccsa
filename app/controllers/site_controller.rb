# -*- encoding : utf-8 -*-

class SiteController < ApplicationController
  layout 'site'

  def home
  	@notices = NoticeDecorator.decorate_collection(Notice.recent.limit(5))
    @thesis = NoticeDecorator.decorate_collection(Notice.recent.where(category: "Teses e Dissertações"))
    @journal = NoticeDecorator.decorate_collection(Notice.recent.where(category: "Revista"))
    @events = NoticeDecorator.decorate_collection(Notice.recent.where(category: "Próximos eventos"))
  end

end
