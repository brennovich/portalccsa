# -*- encoding : utf-8 -*-
class ContactsController < ApplicationController
  def show
    @contact = Contact.new
  end

  def send_message
    @contact = Contact.new(params[:contact])
    @contact.save
    
    redirect_to root_path
  end
end
