# -*- encoding : utf-8 -*-
class AlbumsController < ApplicationController
  def index
    @albums = Album.order('name ASC')
    respond_with(@albums)
  end

  def show
    @album = Album.find(params[:id])
    respond_with(@album)
  end

  def new
    @album = Album.new
    respond_with(@album)
  end

  def edit
    @album = Album.find(params[:id])
  end

  def create
    @album = Album.new(params[:album])
    @album.save
    respond_with(@album)
  end

  def update
    @album = Album.find(params[:id])
    @album.update_attributes(params[:album])
    respond_with(@album)
  end

  def destroy
    @album = Album.find(params[:id])
    @album.destroy
    respond_with(@album)
  end
end
