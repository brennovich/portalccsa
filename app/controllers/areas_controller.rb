# -*- encoding : utf-8 -*-
class AreasController < ApplicationController
  def show
    @area = Area.find(params[:area])

    redirect_to root_path if @area.default?
  end

  def show_page
    @area = Area.find(params[:area])
    @menu = @area.menus.find(params[:menu])
    @page = @menu.pages.find(params[:page])
  end

end
