# -*- encoding : utf-8 -*-
class DocumentsController < ApplicationController
  def index
    query = params[:search].presence || ''
    @tags = Document.tag_counts_on('tags')

    if params[:tag]
      @documents = Document.by_tag(params[:tag]).by_name(query)
    else
      @documents = Document.by_name(query)
    end
  end
end
