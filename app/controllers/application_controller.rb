# -*- encoding : utf-8 -*-
require 'application_responder'

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery

  layout 'site'
end
