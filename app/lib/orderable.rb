# -*- encoding : utf-8 -*-
module Orderable
  def self.included(base)
    base.class_eval do
      attr_accessible :order_number
      scope :ascending, order('order_number ASC')
      scope :descending, order('order_number DESC')
    end
  end
end
