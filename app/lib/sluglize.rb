# -*- encoding : utf-8 -*-
module Sluglize
  def self.included(base)
    base.extend ClassMethods
    base.class_eval do
      validates_format_of :slug, :without => /^\d/
      before_save :generate_slug
    end
    base.send :include, InstanceMethods
  end

  module ClassMethods
    def find(input)
      input.to_i == 0 ? find_by_slug(input) : super
    end
  end

  module InstanceMethods
    def generate_slug
      self.slug = name.parameterize
    end

    def to_param
      slug
    end
  end
end
